package com.inscomp.PolicyAPI.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "client_details_master")
public class ClientDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "client_id")
    private Long client_id;
    @OneToMany
    private List<ApplicationDetails> app_id;
    @Column(name = "client_name")
    private String client_name;
    @Column(name = "client_age")
    private String client_age;
    @Column(name = "client_sex")
    private String client_sex;
    @Column(name = "client_smoker")
    private boolean client_smoker;

    public ClientDetails() {
    }

    public ClientDetails(Long client_id, List<ApplicationDetails> app_id, String client_name, String client_age, String client_sex, boolean client_smoker) {
        this.client_id = client_id;
        this.app_id = app_id;
        this.client_name = client_name;
        this.client_age = client_age;
        this.client_sex = client_sex;
        this.client_smoker = client_smoker;
    }

    public Long getClient_id() {
        return client_id;
    }

    public void setClient_id(Long client_id) {
        this.client_id = client_id;
    }

    public List<ApplicationDetails> getApp_id() {
        return app_id;
    }

    public void setApp_id(List<ApplicationDetails> app_id) {
        this.app_id = app_id;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getClient_age() {
        return client_age;
    }

    public void setClient_age(String client_age) {
        this.client_age = client_age;
    }

    public String getClient_sex() {
        return client_sex;
    }

    public void setClient_sex(String client_sex) {
        this.client_sex = client_sex;
    }

    public boolean isClient_smoker() {
        return client_smoker;
    }

    public void setClient_smoker(boolean client_smoker) {
        this.client_smoker = client_smoker;
    }
}
