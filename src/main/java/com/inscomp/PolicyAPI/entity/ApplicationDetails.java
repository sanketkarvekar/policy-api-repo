package com.inscomp.PolicyAPI.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "application_details_master")
public class ApplicationDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "app_id")
    private Long app_id;
    @Column(name = "policy_id")
    private Long policy_id;
    @Column(name = "app_type")
    private String app_type;
    @Column(name = "app_state")
    private String app_state;
    @Column(name = "app_face_amount")
    private String app_face_amount;
    @ManyToOne
    private Set<ClientDetails> client;

    public ApplicationDetails() {
    }

    public ApplicationDetails(Long app_id, Long policy_id, String app_type, String app_state, String app_face_amount, Set<ClientDetails> client) {
        this.app_id = app_id;
        this.policy_id = policy_id;
        this.app_type = app_type;
        this.app_state = app_state;
        this.app_face_amount = app_face_amount;
        this.client = client;
    }

    public Long getApp_id() {
        return app_id;
    }

    public void setApp_id(Long app_id) {
        this.app_id = app_id;
    }

    public Long getPolicy_id() {
        return policy_id;
    }

    public void setPolicy_id(Long policy_id) {
        this.policy_id = policy_id;
    }

    public String getApp_type() {
        return app_type;
    }

    public void setApp_type(String app_type) {
        this.app_type = app_type;
    }

    public String getApp_state() {
        return app_state;
    }

    public void setApp_state(String app_state) {
        this.app_state = app_state;
    }

    public String getApp_face_amount() {
        return app_face_amount;
    }

    public void setApp_face_amount(String app_face_amount) {
        this.app_face_amount = app_face_amount;
    }

    public Set<ClientDetails> getClient() {
        return client;
    }

    public void setClient(Set<ClientDetails> client) {
        this.client = client;
    }
}
