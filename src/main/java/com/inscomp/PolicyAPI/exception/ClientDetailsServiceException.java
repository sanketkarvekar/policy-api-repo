package com.inscomp.PolicyAPI.exception;

public class ClientDetailsServiceException extends Exception{

    private static final long serialVersionUID = -8783888454591577596L;

    public ClientDetailsServiceException(String message) {
        super(message);
    }
}
