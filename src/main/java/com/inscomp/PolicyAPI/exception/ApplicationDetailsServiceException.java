package com.inscomp.PolicyAPI.exception;

public class ApplicationDetailsServiceException extends Exception{

    private static final long serialVersionUID = -8783888454591577596L;

    public ApplicationDetailsServiceException(String message) {
        super(message);
    }
}
