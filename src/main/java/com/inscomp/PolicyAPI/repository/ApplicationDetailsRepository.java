package com.inscomp.PolicyAPI.repository;

import com.inscomp.PolicyAPI.entity.ApplicationDetails;
import org.springframework.data.repository.CrudRepository;

public interface ApplicationDetailsRepository extends CrudRepository<ApplicationDetails, Long> {

}
