package com.inscomp.PolicyAPI.repository;

import com.inscomp.PolicyAPI.entity.ClientDetails;
import org.springframework.data.repository.CrudRepository;

public interface ClientDetailsRepository extends CrudRepository<ClientDetails, Long> {
}
